const {User} = require('../models');
const bcrypt = require('bcrypt');
const jwt=require('jsonwebtoken');
require('dotenv').config();

const saltRoundsEnv=process.env.SALTROUNDS_BCRYPT;
const saltRounds= parseInt(saltRoundsEnv)


class AuthController{

    static userRenderRegister(req,res){
        res.render('registerPage');
    }

    static async userRegister(req,res){
        try{
            const {username,password}=req.body;
            const salt=bcrypt.genSaltSync(saltRounds);
            const hash=bcrypt.hashSync(password,salt);

            const inputUser= await User.create({
                username:username,
                password:hash
            })
            res.redirect('/login');
            
            // console.log(inputUser);

        }catch(err){
            console.log(err);
        }
    }

    static userRenderLogin(req,res){
        res.render('loginPage')
    }

    static generateToken(id, username,isAdmin){
        const payload={
            id,
            username,
            isAdmin
        }

        const rahasia = process.env.SECRET_KEY;
        const token= jwt.sign(payload,rahasia)
        console.log(token,'==> ini token');
        return token;
    }

    static async userLoginAuth(username,password){
        try{
            const userDatabase=await User.findOne({
                where:{
                    username
                }
            })

            if(!userDatabase) return Promise.reject("Username not found")

            const cekPassword= bcrypt.compareSync(password, userDatabase.password);

            if(!cekPassword) return Promise.reject("password salah");

            return Promise.resolve(userDatabase)

        }catch(errror){
            console.log(error);
        }
    }

    static async userLogin(req,res){
        try{
            const {username,password}=req.body;
            const cekLoginAuth= await AuthController.userLoginAuth(username,password);
            let idUser=cekLoginAuth.dataValues.id;
            let usernameUser=cekLoginAuth.dataValues.username;
            let isAdminUser=cekLoginAuth.dataValues.isAdmin;
            let generateTokenManipulate=await AuthController.generateToken(idUser,usernameUser,isAdminUser)

            // res.redirect('/testauthenticate')
            res.status(200).json({"access_token": generateTokenManipulate})

        }catch(error){
            console.log(error)
        }
    }

    static whoAmI(req,res){
        const currentUser= req.user;
        console.log(currentUser,"==>> masuk whoami2")
        // res.render('roomPage',{isAdmin})
        res.json(currentUser);
    }

}

module.exports=AuthController;