const passport= require('passport');
const localStrategy= require("passport-local").Strategy;
const {Strategy: JwtStrategy, ExtractJwt} = require('passport-jwt')
const AuthController = require('../controllers/AuthController');
const userLogin = AuthController.userLogin;
const {User}= require('../models')
const userLoginAuth=AuthController.userLoginAuth;
require('dotenv').config();


const options = {
    // Untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
    jwtFromRequest : ExtractJwt.fromHeader('authorization'),
    /* Harus sama seperti dengan apa yang kita masukkan sebagai parameter kedua dari jwt.sign di User Model.
    Inilah yang kita pakai untuk memverifikasi apakah tokennya dibuat oleh sistem kita */
    secretOrKey : process.env.SECRET_KEY ,
}
console.log("ini passport")
passport.use(new JwtStrategy(options, async (payload, done) => {
    // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign

    console.log(payload, "ini payload")
    if(payload){
        delete payload.iat;
        done(null, payload)
    }else{
        done("data tidak ditemukan", null)
    }

    User.findByPk(payload.id)
    .then(data => done(null,data))
    .catch(err => done(err,false))
    
    // console.log(payload, "INI PAYLOAD PASSPORT JS")
    // const cekUser=await User.findByPk(payload.id)
    // console.log(cekUser, "INI CEKUSER")
    // if(cekUser) done(null,cekUser)
    // return done("error di cekuser",null)
}))


module.exports=passport;